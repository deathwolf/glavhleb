var blocks = {};

$(function() {
  initBlocks();
});

/**
 * Initialize all blocks on page
 */
function initBlocks() {
  for (var key in blocks) {
    if (blocks[key]) {
      if ($('.' + key).length && blocks[key].init) {
        blocks[key].init();
      }
    }
  }
}

var isMobile = document.body.clientWidth < 640;

function resize() {
  isMobile = document.body.clientWidth < 640;
}

$(window).on('resize', resize);
