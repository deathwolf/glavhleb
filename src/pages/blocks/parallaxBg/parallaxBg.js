blocks.parallaxBg = {
    self: '.parallaxBg',
    limit: '',
    windowHeight: '',
    headerHeight: '',
    currentOffsetTop: [],

    init: function() {
        var self = this;

        !isMobile ? self.parallaxed() : null;

        $(window).on('resize', function() {
            !isMobile ? self.parallaxed() : null;
        })

    },

    parallaxed: function() {
        var self = this;

        var $parallaxBg = $(self.self);

        self.parallaxCalcValues($parallaxBg);

        $(window)
        .on("resize", function() {
            self.parallaxCalcValues($parallaxBg);
        })
        .on('scroll', function() {
            var st = $(this).scrollTop();

            $parallaxBg.each(function (index) {
                var checkpointStart = self.currentOffsetTop[index] - self.windowHeight - self.headerHeight,
                    checkpointEnd = self.currentOffsetTop[index] + self.limit,
                    yPos = -(st - checkpointStart),
                    speed = $(this).data('speed') || 10,
                    coords = ((self.limit + yPos) * 3) / speed + 'px';


                if(st >= checkpointStart && st <= checkpointEnd){
                    $(this).css('background-position', '50% '+coords+'');
                }
            });
        });

    },

    parallaxCalcValues: function (elem) {
        var self = this;

        self.limit = elem.height(),
        self.windowHeight = $(window).height(),
        self.headerHeight = $('.main-nav__item').is('div') ?
                            $('.mainHeader').outerHeight(true) + $('.main-nav__item._active .main-subnav').outerHeight(true):
                            $('.mainHeader').outerHeight(true),
        self.currentOffsetTop = [];

        elem.each(function (index) {
            self.currentOffsetTop.push(parseInt(Math.floor($(this).offset().top)));
        });

        elem.each(function (index, item) {
            var checkpointStart = self.currentOffsetTop[index] - self.windowHeight - self.headerHeight,
                st = $(window).scrollTop(),
                yPos = -(st - checkpointStart),
                speed = elem.data('speed') || 10,
                coords = ((self.limit + yPos) * 3) / speed + 'px';

            $(item).data('start') === true && $(item).css('background-position', '50% '+coords+'');
        });
    },

};
