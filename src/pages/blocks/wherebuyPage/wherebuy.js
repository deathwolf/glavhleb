blocks.wherebuy = {
    map: 'wherebuy-map',
    places: 'wherebuy-places',
    placesItem: '.wherebuy-list-item',
    listPlaces: '.wherebuy-list',
    listDetail: 'wherebuy-list-detail',
    listDetailTopInfo: '.wherebuy-list-detail__desc-top',
    listDetailArticles: '.wherebuy-list-detail-items ul',
    listDetailClose: '.wherebuy-list-detail__close',
    listDetailInfoType: '.wherebuy-list-detail-info',
    mobOpenPlaces: '.wherebuy-places-button-list',
    openDetail: false,
    activeDetailIndex: null,
    wherebuyPlacesScroll: [],
    wherebuyListDetailScroll: [],
    gMap: [],
    bounds: [],
    icon: {},
    hoveredIcon: {},
    items: [],
    markers: [],
    rightMargin: 50,
    offset: null,
    devices: document.body.clientWidth < 1024,

    init: function () {
        var self = this;

        $(window).on('resize', function () {
            self.devices = document.body.clientWidth < 1024;
        })

    },

    initMap: function(offset, filter) {
        var self = this;
        var myLatLng = {lat: 55.751244, lng: 37.618423};

        // var isDraggable = !isMobile ? true : false;

        self.offset = !!offset;

        var mapOptions = {
            center: myLatLng,
            scrollwheel: false,
            draggable: true,
            zoom: 10,
            disableDefaultUI: true,
            zoomControl: true,
            clickableIcons: false,
        };


        var directionsDisplay = new google.maps.DirectionsRenderer({
            draggable: true
        });
        var directionsService = new google.maps.DirectionsService();

        self.bounds = new google.maps.LatLngBounds();
        self.offset = !self.offset ? document.body.clientWidth - $('.'+self.places).offset().left : $('.modal-dialog').innerWidth() - $('.'+self.places).offset().left;
        if (typeof(self.offset) == 'undefined') self.offset = 0;

        self.gMap = new google.maps.Map(document.getElementById(self.map), mapOptions);
        directionsDisplay.setMap(self.gMap);

        self.icon = {
            url: 'images/svg/wherebuy-mark.svg',
            scaledSize: new google.maps.Size(16, 22),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(8, 22)
        };
        self.hoveredIcon = {
            url: 'images/svg/wherebuy-mark.svg',
            scaledSize: new google.maps.Size(42, 59),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(21, 59)
        };


        $.getJSON('scripts/places.json', function(data){
            $.each(data.places, function(i, item){

                var currentItem = item;

                if (typeof(filter) == 'number' && typeof(filter) !== 'undefined' && filter != null){
                    $.each(item.moreInfo.items, function(i, item) {
                        if (item.id == filter)
                            self.renderMarkers(i, currentItem)
                    });
                } else {
                    self.renderMarkers(i, item)
                }


            });
        }).done(function () {

            self.initPlacesScroll();
            self.openPlacesInfo(self.items);

            self.gListeners(self.bounds, self.offset);
        });

    },

    renderMarkers: function (i, item) {
        var self = this;

        var markerPos = new google.maps.LatLng(item.coordinates[0], item.coordinates[1]);

        var marker = new google.maps.Marker({
            map: self.gMap,
            position: markerPos,
            scale: 10,
            icon: self.icon
        });

        google.maps.event.addListener(marker, 'mouseover', function() {
            if (!self.openDetail) {
                this.setIcon(self.hoveredIcon);
                $(self.placesItem).eq(i).addClass('_active');
            }
        });

        google.maps.event.addListener(marker, 'mouseout', function() {
            if (!self.openDetail) {
                this.setIcon(self.icon);
                $(self.placesItem).eq(i).removeClass('_active');
            }
        });

        google.maps.event.addListener(marker, 'mousedown', function () {
            if (!self.openDetail) {
                self.openDetail = true;
                self.activeDetailIndex = i;
                if(self.devices) {
                    $('.'+self.places).addClass('_open');
                } else {
                    $('.'+self.listDetail).addClass('_active');
                    $('.'+self.places).removeClass('_open');
                }
                $(self.placesItem).eq(i).removeClass('_active');
                self.renderPlacesDetail(i, item);
            }
        });


        self.bounds.extend(markerPos);

        self.items.push(item);
        self.markers.push(marker);

        self.renderPlacesItems(item);
    },

    gListeners: function (bounds, offset) {
        var self = this;

        google.maps.event.addListenerOnce(self.gMap, 'idle', function() {
            self.fitAndOffsetMap(self.gMap, bounds, offset);
        });

        google.maps.event.addListener(self.gMap, 'bounds_changed', function () {
            self.showVisibleMarkers(self.gMap);
        });

        google.maps.event.addDomListener(window, 'resize', function() {
            google.maps.event.trigger(self.gMap, 'resize');
            self.fitAndOffsetMap(self.gMap, bounds, offset);
        });
    },

    zoomAlign: function (map, bounds) {
        var self = this;

        var newSpan = map.getBounds().toSpan();
        var askedSpan = bounds.toSpan();
        var latRatio = (newSpan.lat()/askedSpan.lat()) - 1;
        var lngRatio = (newSpan.lng()/askedSpan.lng()) - 1;

        if (Math.min(latRatio, lngRatio) > 0.46) {
          map.setZoom(map.getZoom() + 1);
        }
    },

    offsetMap: function (map, bounds, offset) {
        var self = this;

        var topRightCorner = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng());

        var topRightPoint = self.fromLatLngToPoint(map, topRightCorner).x;

        var leftCoords = bounds.getSouthWest();
        var rightCoords = bounds.getNorthEast();
        var leftMost = self.fromLatLngToPoint(map, leftCoords).x;
        var rightMost = self.fromLatLngToPoint(map, rightCoords).x;

        var leftOffset = leftMost - offset - self.rightMargin;
        var rightOffset = ((topRightPoint - self.rightMargin) - rightMost);


        if (document.body.clientWidth >= 1024) {

            if (rightOffset + leftOffset >= 0) {

                var mapOffset = Math.round((rightOffset - leftOffset));
                var mapOffsetStable = Math.round((rightOffset));
                var newLeftPoint = self.fromLatLngToPoint(map, leftCoords).x - offset;

                if (newLeftPoint <= self.rightMargin) {
                    map.panBy(mapOffsetStable, 0);
                } else {
                    map.panBy(mapOffset, 0);
                }
            } else {
                map.setZoom(map.getZoom() - 1);
                self.offsetMap(map, bounds, offset);
            }
        }
    },

    fromLatLngToPoint: function (map, latLng) {

        var scale = Math.pow(2, map.getZoom());
        var nw = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getSouthWest().lng());
        var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
        var worldCoordinate = map.getProjection().fromLatLngToPoint(latLng);

        return new google.maps.Point(Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale), Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale));
    },

    fitAndOffsetMap: function (map, bounds, offset) {
        var self = this;

        map.fitBounds(bounds);
        self.offsetMap(map, bounds, offset);
    },

    showVisibleMarkers: function (map) {
        var self = this;
        var bounds = map.getBounds();

        $.each(self.markers, function (i, item) {
            var marker = item,
                $placesItem = $(self.placesItem).eq(i);

            bounds.contains(marker.getPosition()) === true ?
                $placesItem.removeClass('_invisible') :
                $placesItem.addClass('_invisible');
        });

        self.wherebuyPlacesScroll.onResize();
    },

    renderPlacesItems: function (item) {
        var self = this;

        $(self.listPlaces).append('\
            <a href="" class="wherebuy-list-item">\
                <span class="wherebuy-list-item__title">'+ self.checkValue(item.title) +'</span>\
                <span class="wherebuy-list-item__address">'+ self.checkValue(item.address) +'</span>\
            </a>');
    },

    renderInfoLines: function (elem, link, linkType) {
        var self = this;

        var arrayLines = [];
        $.each(elem, function (i, item) {
            linkType = linkType ? linkType : '';
            if (!$.isEmptyObject(item)) {
                if (link == true) {
                    arrayLines += '<a href="'+ linkType + item +'">'+ item +'</a><br />'
                }
                else if (link == 'item') {
                    arrayLines += '<a href="'+ linkType + item.url +'">'+ item.title +'</a><br />'
                }
                else {
                    arrayLines += '<span>'+ item +'</span><br />'
                }
            }
        });

        return arrayLines;
    },

    renderPlacesDetail: function (index, item, close) {
        var self = this;

        close = !!close;



        if (!close) {
            self.openDetail = true;
            self.activeDetailIndex = index;
            var modif = item.typeInfo ? '_info' : '';
            self.markers[self.activeDetailIndex].setIcon(self.hoveredIcon);
            $('.'+self.listDetail)
                .addClass('_active ' + modif)
                .one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function () {
                    self.wherebuyListDetailScroll.onResize();
                });

            $(self.listDetailTopInfo).append('\
                <span class="wherebuy-list-detail__title">'+ self.checkValue(item.title) +'</span>\
                <span class="wherebuy-list-detail__address">'+ self.checkValue(item.address) +'</span>\
            ');
            if (item.typeInfo) {
                $(self.listDetailInfoType).clone().insertAfter('.wherebuy-list-detail__title');
            }
            if (item.moreInfo) {
                $(self.listDetailTopInfo).append('\
                    <span class="wherebuy-list-detail__worktime">'+ self.renderInfoLines(item.moreInfo.worktime) +'</span>\
                    <span class="wherebuy-list-detail__phone">'+ self.renderInfoLines(item.moreInfo.phones, true, 'tel:') +'</span>\
                    <span class="wherebuy-list-detail__site">'+ self.renderInfoLines(item.moreInfo.site, 'item') +'</span>\
                    ');
                    $(self.listDetailArticles).append('\
                    <li>'+ self.renderInfoLines(item.moreInfo.items, 'item') +'</li>\
                ');
            }


        } else {
            self.openDetail = false;
            var modif = self.items[self.activeDetailIndex].typeInfo ? '_info' : '';
            $('.'+self.listDetail)
                .removeClass('_active')
                .removeClass(modif)
                .one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',
                function() {
                    $(self.listDetailTopInfo +', '+ self.listDetailArticles).html('');
                    self.wherebuyListDetailScroll.update();
                });

            self.markers[self.activeDetailIndex].setIcon(self.icon);
            self.wherebuyPlacesScroll.onResize();

            if (self.devices) {
                if (!$('.'+self.places + '> .swiper-container').hasClass('_open'))
                    $('.'+self.places).removeClass('_open');
            }
        }
    },

    openPlacesInfo: function (item) {
        var self = this;

        $(self.placesItem)
            .on('click', function (e) {
                e.preventDefault();

                var index = $(this).index();
                self.renderPlacesDetail(index, item[index])
            })
            .on('mouseenter', function () {
                !self.openDetail &&
                    self.markers[$(this).index()].setIcon(self.hoveredIcon);
            })
            .on('mouseleave', function () {
                !self.openDetail &&
                    self.markers[$(this).index()].setIcon(self.icon);
            });

        $(self.listDetailClose).on('click', function (e) {
            e.preventDefault();
            self.renderPlacesDetail(self.activeDetailIndex, item, true)
        });

        $(self.mobOpenPlaces).on('click', function (e) {
            e.preventDefault();

            if (self.openDetail) {
                var modif = self.items[self.activeDetailIndex].typeInfo ? '_info' : '';

                $('.'+self.places + '> .swiper-container').addClass('_open');
                $('.'+self.listDetail).removeClass('_active').removeClass(modif);
                self.renderPlacesDetail(self.activeDetailIndex, item, true)
            } else {
                $(this).next().toggleClass('_open');
                $(this).next().find('> .swiper-container').toggleClass('_open');
            }
        })

    },

    initPlacesScroll: function () {
        var self = this;

        var scrollPage,
            mouseWheelSens = 1;

        if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){ // firefox detect
            mouseWheelSens = 20;
        }

        self.wherebuyPlacesScroll = new Swiper('.'+self.places+' > .swiper-container', {
            scrollbar: '.wherebuy-scrollbar',
            direction: 'vertical',
            scrollbarHide: false,
            scrollbarDraggable: true,
            slidesPerView: 'auto',
            mousewheelControl: true,
            freeMode: true,
            mousewheelSensitivity: mouseWheelSens,
            simulateTouch: false,
        });

        self.wherebuyListDetailScroll = new Swiper('.'+self.listDetail+' .swiper-container', {
            scrollbar: $('.wherebuy-scrollbar-detail').get(0),
            direction: 'vertical',
            scrollbarHide: false,
            scrollbarDraggable: true,
            slidesPerView: 'auto',
            mousewheelControl: true,
            freeMode: true,
            mousewheelSensitivity: mouseWheelSens,
            simulateTouch: false,
        });

        if (!self.devices) {
            $('.'+self.places)
            .on('mouseenter', function (e) {
                $('body').on({
                    'scroll mousewheel': function(e) {
                        if ($(e.target).hasClass(self.places)) return;
                        e.preventDefault();
                        e.stopPropagation();
                    }
                });
            })
            .on('mouseleave', function (e) {
                $('body').unbind('scroll mousewheel');
            })
        }
    },

    checkValue: function (elem) {
        var self = this;

        return elem || '';
    },

    clearMap: function () {
        var self = this;

        google.maps.event.clearListeners(self.gMap, 'bounds_changed')

        self.openDetail = false;
        self.activeDetailIndex = null;
        self.gMap = null;
        self.bounds  = [];
        self.items  = [];
        self.markers = [];

    },
};
