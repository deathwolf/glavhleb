blocks.mainHeader = {
    self: '.mainHeader',
    mobBtn: '.mob-menu',

    init: function() {
        var self = this;

        $(self.mobBtn).on('click', function () {
            $(this).toggleClass('_open');
        })
    },

};
