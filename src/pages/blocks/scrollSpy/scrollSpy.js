blocks.scrollSpy = {
    nav: '.main-nav',
    scrollSpyTarget: '.scrollSpy',

    init: function () {
        var self = this;

        self.initScrollSpy();
    },

    initScrollSpy: function () {
        var self = this;

        var offsetHeaderHeight = $(self.nav).outerHeight(true) + $(self.scrollSpyTarget).outerHeight(true);

        $('body').scrollspy({
            target: self.scrollSpyTarget,
            offset: offsetHeaderHeight,
        });

        $(self.scrollSpyTarget + ' a[href^="#"]').on('click', function(e) {
           e.preventDefault();
        
           var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 'slow', function() {
                window.location.hash = '';
            });

        });
    },
};
