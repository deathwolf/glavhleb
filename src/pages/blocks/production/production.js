blocks.productionDetail = {
    slider: '.production-slider',
    carousel: '.production-carousel',

    init: function () {
        var self = this;

        var productSlider = new Swiper(self.slider+' .swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            grabCursor: true,
        });

        var productCarousel = new Swiper(self.carousel+' .swiper-container', {
            slidesPerView: 4,
            spaceBetween: 50,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            grabCursor: true,
        });

    },
};
