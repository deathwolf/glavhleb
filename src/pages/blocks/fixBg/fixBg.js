blocks.fixBg = {
    self: '.mainHeader',
    fixBg: '.fixBg',

    init: function() {
        var self = this;

        self.fixHeader();
        self.parallaxed();

    },

    fixHeader: function () {
        var self = this;

        $(self.self).affix({
            offset: {
                top: function () {
                    return $(self.fixBg).height() - $(self.self).innerHeight();
                }
            }
        });
    },

    parallaxed: function() {
        var self = this;

        var $fixBg = $(self.fixBg),
            $fixBgLogo = $fixBg.find('.logo')
            limit = $fixBg.height();

        $(window).on('scroll', function() {
            var st = $(this).scrollTop(),
                yPos = -((st * 0.5) / 2),
                coords = yPos + 'px';

            if(st <= limit){
                $fixBg.css('transform', 'translate3d(0,'+coords+',0)');
                // $fixBgLogo.css({
                //     'opacity': (1 - st/limit)
                // });
            }
        });
    },
};
