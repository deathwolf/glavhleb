blocks.contacts = {
    map: 'contacts-map',

    init: function () {
        var self = this;

    },
    initMap: function() {
        var self = this;
        var myLatLng = {lat: 55.777241, lng: 37.704681};

        var isDraggable = !isMobile ? true : false;

        var mapOptions = {
            center: myLatLng,
            scrollwheel: false,
            draggable: isDraggable,
            zoom: 17,
            disableDefaultUI: true,
            zoomControl: true,
        };

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById(self.map), mapOptions);

        // Create a marker and set its position.
        var marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            icon: 'images/brand-mark-map.png'
        });

        map.panTo(marker.getPosition());
    },

};
