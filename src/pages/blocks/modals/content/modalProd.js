blocks.modalProd = {
    self: '.modalProd',
    activateLink: '.link-wherebuy',
    recallEvents: false,

    init: function () {
        var self = this;

        $(document).on('shown.bs.modal', self.self, function(e){

            var link = $(e.relatedTarget);

            var filter = $(link).data('filter');
            $.ajax(link.attr('href')).done(function () {
                if ($.isEmptyObject(blocks.wherebuy.gMap)) {
                    blocks.wherebuy.initMap(true, filter);
                    self.recallEvents && blocks.wherebuy.gListeners(blocks.wherebuy.bounds, blocks.wherebuy.offset);
                }
            });
        });

        $(document).on('hidden.bs.modal', self.self, function () {
            $(this).removeData('bs.modal');
            self.recallEvents = true;

            blocks.wherebuy.clearMap();
        });
    },
}
