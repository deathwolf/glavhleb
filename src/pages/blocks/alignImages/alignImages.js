blocks.alignImages = {
    self: '.alignImages',
    itemInner: '.image-content__item',

    init: function () {
        var self = this;

        self.calculateHandler();

        $(window).on('resize', function () {
            self.calculateHandler();
        });
    },

    calculateHandler: function () {
        var self = this;

        var marg = 50;

        if(document.body.clientWidth < 640) {
            marg = 20;
        }
        else if(document.body.clientWidth < 1024) {
            marg = 30;
        } else {
            marg = 50;
        }

        $('._half._long + ._half + ._half._long + ._half').each(function() {
            var $this = $(this),
                outerHeight = $this.find(self.itemInner).outerHeight(true) + marg;

            $this.css('margin-top', -outerHeight);
        });
    }
};
