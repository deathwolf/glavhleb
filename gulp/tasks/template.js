module.exports = function (options) {

	gulp.task('template', function () {

		var jadeOptions = {
			locals: require('../../data/locals.json')
		};

		return gulp.src(options.files.pages)
			.pipe($.plumber({errorHandler: options.errorHandler("template")}))
			//.pipe($.cached('jade'))
			//TODO gulp-data
			.pipe($.jade(jadeOptions))
			.pipe($.prettify({indent_size: 4}))
			.pipe(gulp.dest(options.paths.dist))
			.pipe(browserSync.stream());
	});

};
