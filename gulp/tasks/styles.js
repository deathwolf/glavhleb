var rupture = require('rupture'),
	ensure = require('./ensure.js');

module.exports = function (options) {

	gulp.task('styles', function () {


		return gulp.src([
			ensure('node_modules/swiper/dist/css/swiper.css'),
			options.files.sass,
		])
			.pipe($.concat('styles.css'))
			.pipe($.plumber({errorHandler: options.errorHandler("styles")}))
			.pipe($.bulkSass())
			.pipe($.sass())
			.pipe($.autoprefixer({
				browsers: ['> 1%'],
				cascade: false
			}))
			.pipe($.minifyCss())
			.pipe($.csscomb())
			.pipe($.rename({basename: 'styles'}))
			.pipe(gulp.dest(options.paths.dist + '/styles'))
			.pipe(browserSync.stream());
	});

};
