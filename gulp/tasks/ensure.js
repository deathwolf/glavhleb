var fs = require('fs');

module.exports = exports = function(path) {
  try {
    fs.lstatSync(path);
    return path;
  }
  catch (e) {
    return '../' + path;
  }
};
