var ensure = require('./ensure.js'),
	runSequence = require('run-sequence');

module.exports = function(options) {

	gulp.task('jshint', function() {
		return (
			gulp
				.src([
					options.files.jsComponents,
					options.files.js,
					'gulp/tasks/*.js',
					'gulpfile.js'
				])
				.pipe($.jshint())
				.pipe($.jshint.reporter())
		);
	});

	gulp.task('scripts', function() {
		gulp.src([
			options.files.json,
		])
			.pipe(gulp.dest(options.paths.dist + '/scripts'))

		return (
			gulp
				.src([
					ensure('node_modules/jquery/dist/jquery.js'),
					ensure('node_modules/bootstrap/dist/js/bootstrap.js'),
					ensure('node_modules/swiper/dist/js/swiper.js'),
					options.files.js,
					options.files.jsComponents,
				])
				.pipe($.concat('app.js'))
				.pipe(options.release ? $.uglify() : $.util.noop())
				.pipe(gulp.dest(options.paths.dist + '/scripts'))

		);

	});

	return function(callback) {
		runSequence('jshint', 'scripts', callback);
	};
};
