module.exports = function (options) {
    gulp.task('assets', function () {
        gulp.src([
            options.files.fonts
        ])
            .pipe(gulp.dest(options.paths.dist + '/fonts'))
            .pipe(browserSync.stream());

        gulp.src([
            options.files.svg
        ])
            .pipe(gulp.dest(options.paths.dist + '/images/svg'))
            .pipe(browserSync.stream());

        return gulp.src([
            options.files.images,
            options.files.imagesSource,
        ])
            .pipe($.plumber({ errorHandler: options.errorHandler('assets') }))
            .pipe(gulp.dest(options.paths.dist + '/images'))
            .pipe(browserSync.stream());
	})
}
