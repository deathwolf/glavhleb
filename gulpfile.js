var gulp = global.gulp = require('gulp');
var $ = global.$ = require('gulp-load-plugins')({
	rename: {
		'gulp-sass-bulk-import': 'bulkSass'
	}
});
global.browserSync = require('browser-sync');

var gutil = require('gulp-util');
var wrench = require('wrench');
var args = require('yargs').argv;

var options = {
	files: {
		js: 'src/scripts/**/*.js',
		jsComponents: 'src/pages/blocks/**/*.js',
		json: 'src/scripts/*.json',
		html: 'src/**/*.html',
		jade: 'src/pages/**/*.jade',
		pages: 'src/pages/**.jade',
		sass: 'src/styles/app.scss',
		sassAll: 'src/**/*.scss',
		style: 'styles/**/*.css',
		images: 'src/images/*.{jpg,jpeg,gif,png,ico}',
		imagesSource: 'src/images/source/**',
		svg: 'src/images/svg/**',
		sprite: 'src/images/sprite/*.*',
		fonts: 'src/fonts/**'
	},
	paths: {
		src: 'src',
		test: 'test',
		dist: 'dist',
		tmp: '.tmp',
		e2e: 'e2e'
	},
	release: 'r' in args || 'release' in args,

	errorHandler: function (title) {
		return function (err) {
			gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
			this.emit('end');
		};
	}
};

wrench.readdirSyncRecursive('./gulp/tasks').filter(function (file) {
	return (/\.(js|coffee)$/i).test(file);
}).map(function (file) {
	require('./gulp/tasks/' + file)(options);
});

gulp.task('default', ['clean'], function () {
	gulp.start('build');
});
